package com.register.registration.RegisterRepo;

import com.register.registration.RegisterClass.RegisterClass;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RegisterRepo extends JpaRepository<RegisterClass,Integer> {


}
