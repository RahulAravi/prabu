package com.register.registration.RegisterClass;


import jakarta.persistence.*;
import jakarta.persistence.criteria.CriteriaBuilder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.autoconfigure.web.WebProperties;

@Data
@AllArgsConstructor
@NoArgsConstructor

@Entity
@Table(name="Register")


public class RegisterClass {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="S.No")
    private Integer sno;
    @Column(name="First Name")
    private String firstname;
    @Column(name="Last Name")
    private String lastname;
    @Column(name="DOB")
    private String dateofbirth;
    @Column(name="Mobile No")
    private Long mobileNo;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getDateofbirth() {
        return dateofbirth;
    }

    public void setDateofbirth(String dateofbirth) {
        this.dateofbirth = dateofbirth;
    }

    public Long getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(Long mobileNo) {
        this.mobileNo = mobileNo;
    }
}
